<?php
require("./_connect.php");

session_Start();
// connect to db
$db = new mysqli($db_host, $db_user, $db_password, $db_name);
$username_session = $_SESSION['username'];
if ($db->connect_errno) {
	// if the connection to the db failed
	echo "Failed to connect to MySQL: (". $db->connect_errno .") ". $db->connect_error;
}

$query="SELECT * FROM chat ORDER BY id ASC";
// execute query
if ($db->real_query($query)) {
	// if the query was successful
	$res = $db->use_result();

	while ($row = $res->fetch_assoc()) {
		$username=$row["username"];
		$text=$row["text"];
		$time=date('G:i', strtotime($row["time"])); // outputs date as # #Hour#:#Minute#
		
		if ($username == $username_session)
		{
			echo "<div style='width: 100%; display: flex; flex-direction: row; justify-content: flex-end;'>
					<div style='
						background-color: rgba(153, 204, 0, 0.9);
						text-align: right;
						width: 400px;
						margin: 5px;
						margin-right: 5px;
						fload: right;
						padding: 5px;
						border-radius: 20px; 
						border: none;'>
					<p style='border: none;'>
						<span style='color: #666699; font-size: 13px; font-weight: bold;'>$username</span>
						<span style='font-size: 10px; color: #797979'>$time</span>
					</p>
					<p style='font-size: 20px; font-weight: 100; color: #edeff8; border: none'>$text</p>
				      </div>
				</div>\n";
		} else
		{
			echo "<div style='
				background-color: #ffd9cc;
				text-align:left;
				border: none;
				position: relative;
				width: 400px;
				margin: 5px;
				padding: 5px;
				border-radius: 20px;'>
				<p style='border: none;'>
					<span style='font-size: 10px; color: #797979;'>$time</span>
					<span style='color: #666699; font-size: 13px; font-weight: bold;'>$username</span>
				</p>
				<p style='font-weight: 100; font-size: 20px; color: #666699; border: none'>$text</p></div>\n";
		}
	}
} else {
	// If the query was NOT successful
	echo "An error occured";
	echo $db->errno;
}

$db->close();
?>
