<?php
	session_start();
	if (!isset($_SESSION['username']))
	{
		header('Location: ../index.php');
	}
	else
	{
		$username = $_SESSION['username'];
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="../css/main.css" type="text/css" media="screen" />
	</head>
	<body>	
		<div class="container">
			<header class="header">
				<h1 style="float: left">Welcome <?php echo $username ?>!</h1>
				<button title="Logout from <?php echo $username ?>" id="logout">logout</button>
			</header>
			<main>
				<div class="userSettings">
					<input id="userName" type="hidden" placeholder="Username" value="<?php echo $username ?>">
					<div class="chat">
						<div id="chatOutput"></div>
					</div>
					<input autofocus id="chatInput" type="text" placeholder="Input Text Here" maxLength="128">
					<button title="Send your message, duh!" id="chatSend">Send</button>
				</div>
			</main>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
			<script src="https://cdn.jsdelivr.net/npm/vue"></script>
			<script src="../js/index.js"></script>
		</div>
	</body>
</html>
